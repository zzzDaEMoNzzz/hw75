const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
app.use(bodyParser.json());
app.use(cors());

const port = 8000;

app.post('/encode', (req, res) => {
  res.send({encoded: Vigenere.Cipher(req.body.password).crypt(req.body.message)});
});

app.post('/decode', (req, res) => {
  res.send({decoded: Vigenere.Decipher(req.body.password).crypt(req.body.message)});
});

app.listen(port, () => {
  console.log('We are live on http://localhost:' + port);
});
import React, { Component } from 'react';
import {connect} from "react-redux";
import {changeValue, decodeRequest, encodeRequest} from "./store/actions";
import './App.css';

class App extends Component {
  onChange = (event, stateName) => {
    event.preventDefault();

    this.props.changeValue(stateName, event.target.value);
  };

  render() {
    return (
      <div className="App">
        <label>Decoded message</label>
        <textarea
          rows="5"
          value={this.props.decodedMessage}
          onChange={event => this.onChange(event, 'decodedMessage')}
        />
        <label>Password</label>
        <div>
          <input
            type="text"
            value={this.props.password}
            onChange={event => this.onChange(event, 'password')}
          />
          <button
            className="encodeBtn"
            disabled={this.props.password.length === 0}
            onClick={() => this.props.encodeRequest(this.props.password, this.props.decodedMessage)}
          >
            encode
          </button>
          <button
            className="decodeBtn"
            disabled={this.props.password.length === 0}
            onClick={() => this.props.decodeRequest(this.props.password, this.props.encodedMessage)}
          >
            decode
          </button>
        </div>
        <label>Encoded message</label>
        <textarea
          rows="5"
          value={this.props.encodedMessage}
          onChange={event => this.onChange(event, 'encodedMessage')}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  encodedMessage: state.encodedMessage,
  decodedMessage: state.decodedMessage,
  password: state.password,
});

const mapDispatchToProps = dispatch => ({
  changeValue: (stateName, value) => dispatch(changeValue(stateName, value)),
  encodeRequest: (password, message) => dispatch(encodeRequest(password, message)),
  decodeRequest: (password, message) => dispatch(decodeRequest(password, message))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

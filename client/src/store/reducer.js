import {CHANGE_VALUE, DECODE_REQUEST_SUCCESS, ENCODE_REQUEST_SUCCESS} from "./actions";

const initialState = {
  encodedMessage: '',
  decodedMessage: '',
  password: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_VALUE:
      return {...state, [action.stateName]: action.value};
    case ENCODE_REQUEST_SUCCESS:
      return {...state, decodedMessage: '', encodedMessage: action.encoded};
    case DECODE_REQUEST_SUCCESS:
      return {...state, encodedMessage: '', decodedMessage: action.decoded};
    default:
      return state;
  }
};

export default reducer;
import axios from 'axios';

export const CHANGE_VALUE = 'CHANGE_VALUE';
export const ENCODE_REQUEST_SUCCESS = 'ENCODE_REQUEST_SUCCESS';
export const DECODE_REQUEST_SUCCESS = 'DECODE_REQUEST_SUCCESS';

export const changeValue = (stateName, value) => ({type: CHANGE_VALUE, stateName, value});

export const encodeRequest = (password, message) => {
  return dispatch => {
    axios.post('http://localhost:8000/encode/', {password, message}).then(response => {
      dispatch({type: ENCODE_REQUEST_SUCCESS, encoded: response.data.encoded});
    });
  };
};

export const decodeRequest = (password, message) => {
  return dispatch => {
    axios.post('http://localhost:8000/decode/', {password, message}).then(response => {
      dispatch({type: DECODE_REQUEST_SUCCESS, decoded: response.data.decoded});
    });
  };
};